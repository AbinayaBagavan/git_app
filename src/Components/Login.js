import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Link,withRouter } from 'react-router-dom';
import {connect} from 'react-redux';
import {updateUserName} from '../Actions/LoginAction.js';
const mapStateToProps = (state) => {
  console.log(state);
  return {
    stateInfo:state
  }
}
const mapDispatchToProps = (dispatch) =>{
  return{
    setUser:(name)=>dispatch(updateUserName(name))
  }
}
class Login extends Component {
  constructor(props){
    super(props);
    this.state = {
      userName : ""
    }
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(event){
    console.log(this.props.stateInfo.userName);
    this.setState({userName : event.target.value})
    this.props.setUser(this.state.userName);

  }
  render() {
    return (
      <div className="App">
        <TextField
          label="Name"
          value={this.state.userName}
          onChange={e => this.handleChange(e)}
          margin="normal"
          variant="outlined"
        /><br/>
        <Button variant="contained" >
              login
        </Button>
        
      </div>
    );
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Login);
