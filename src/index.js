import React from 'react';
import ReactDOM from 'react-dom';
import Login from './Components/Login.js';
//import Profile from './Components/Profile.js';
import './App.css';
import {reducer} from './Reducers/reducer.js';
import { Provider } from 'react-redux';
import { createStore} from 'redux';
import { BrowserRouter as Router, Route ,Switch} from 'react-router-dom'
import {details} from'./InitialState.js'
const store = createStore(reducer,details);

ReactDOM.render(
<Provider store = {store}>
<Router>
    <Switch>
        <Route exact path="/" render = {()=>(<Login/>)}/>
    </Switch>
</Router>
</Provider>, document.getElementById('root'));
